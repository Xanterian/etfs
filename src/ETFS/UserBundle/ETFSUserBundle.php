<?php

namespace ETFS\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ETFSUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
