<?php

namespace ETFS\RACCondBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {

        return $this->render('ETFSRACCondBundle:Default:index.html.twig');
    }
}
