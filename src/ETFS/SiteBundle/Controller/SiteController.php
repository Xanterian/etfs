<?php

namespace ETFS\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SiteController extends Controller
{
    public function indexAction()
    {

        //Si le visteur est déja authentifié on le redirige vers l'accueil.
        //Sinon page de connexion
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->render('@ETFSSite/Default/index.html.twig');
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
        return $this->render('@ETFSSite/Default/index.html.twig');
    }

    public function HeadmenuAction()
    {
        return $this->render('@ETFSSite/Default/HeadMenu.html.twig');
    }

    public function SidemenuAction()
    {
        return $this->render('@ETFSSite/Default/Sidemenu.html.twig');
    }
}
